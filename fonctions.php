<?php

function recuperer_article($pdo, $id_article, $role) {
    $sql = "SELECT * FROM Article WHERE IdArticle = :id_article";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id_article' => $id_article]);
    $article = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($article && $role) {
        $sql = "SELECT * FROM Likes WHERE IdArticle = :id_article";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['id_article' => $id_article]);
        $likes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $compteur_likes = 0;
        $compteur_dislikes = 0;

        foreach ($likes as $like) {
            if ($like['Type'] == 1) {
                $compteur_likes++;
            } else {
                $compteur_dislikes++;
            }
        }

        $article['compteur_likes'] = $compteur_likes;
        $article['compteur_dislikes'] = $compteur_dislikes;

        $article['utilisateurs_likes'] = obtenir_utilisateurs_likes($pdo, $id_article, 1);
        $article['utilisateurs_dislikes'] = obtenir_utilisateurs_likes($pdo, $id_article, 0);
    }

    return $article;
}

function lister_articles($pdo, $role) {
    $sql = "SELECT IdArticle, Auteur, DatePublication, Contenu FROM Article";
    $stmt = $pdo->query($sql);
    $articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($role) {
        foreach ($articles as &$article) {
            $sql = "SELECT COUNT(*) as count FROM Likes WHERE IdArticle = :id_article AND Type = 1";
            $stmt = $pdo->prepare($sql);
            $stmt->execute(['id_article' => $article['IdArticle']]);
            $article['compteur_likes'] = $stmt->fetchColumn();

            $sql = "SELECT COUNT(*) as count FROM Likes WHERE IdArticle = :id_article AND Type = 0";
            $stmt = $pdo->prepare($sql);
            $stmt->execute(['id_article' => $article['IdArticle']]);
            $article['compteur_dislikes'] = $stmt->fetchColumn();

            if ($role == 'moderator') {
                $article['utilisateurs_likes'] = obtenir_utilisateurs_likes($pdo, $article['IdArticle'], 1);
                $article['utilisateurs_dislikes'] = obtenir_utilisateurs_likes($pdo, $article['IdArticle'], 0);
            }
        }
    }

    return $articles;
}


function creer_article($pdo, $post_data) {
    $sql = "INSERT INTO Article (Auteur, DatePublication, Contenu) VALUES (:auteur, NOW(), :contenu)";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute([
        'auteur' => $_SESSION['user_id'],
        'contenu' => $post_data['contenu']
    ]);
}

function modifier_article($pdo, $id_article, $post_data) {
    $sql = "UPDATE Article SET Contenu = :contenu WHERE IdArticle = :id_article AND Auteur = :auteur";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute([
        'contenu' => $post_data['contenu'],
        'id_article' => $id_article,
        'auteur' => $_SESSION['user_id']
    ]);
}


function supprimer_article($pdo, $id_article, $role) {
    $sql = "DELETE FROM Likes WHERE IdArticle = :id_article";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $stmt->execute();

    if ($role == 'moderator') {
        $sql = "DELETE FROM Article WHERE IdArticle = :id_article";
    } else {
        $sql = "DELETE FROM Article WHERE IdArticle = :id_article AND Auteur = :user_id";
    }

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id_article', $id_article, PDO::PARAM_INT);

    if ($role != 'moderator') {
        $stmt->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
    }

    return $stmt->execute();
}

function liker_article($pdo, $id_article, $user_id, $type) {
    $article = recuperer_article($pdo, $id_article, $_SESSION['role']);
    if ($article['Auteur'] == $user_id) {
        return null;
    }

    $sql = "INSERT INTO Likes (IdUtilisateur, IdArticle, Type) VALUES (:user_id, :id_article, :type)
            ON DUPLICATE KEY UPDATE Type = :type";

    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $stmt->bindParam(':type', $type, PDO::PARAM_BOOL);

    return $stmt->execute();
}

function disliker_article($pdo, $id_article, $user_id) {
    return liker_article($pdo, $id_article, $user_id, false);
}

function obtenir_nombre_likes($pdo, $id_article, $type) {
    $sql = "SELECT COUNT(*) as count FROM Likes WHERE IdArticle = :id_article AND Type = :type";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $stmt->bindParam(':type', $type, PDO::PARAM_BOOL);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    return $result['count'];
}

function obtenir_utilisateurs_likes($pdo, $id_article, $type) {
    $sql = "SELECT IdUtilisateur FROM Likes WHERE IdArticle = :id_article AND Type = :type";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id_article', $id_article, PDO::PARAM_INT);
    $stmt->bindParam(':type', $type, PDO::PARAM_BOOL);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

    return $result;
}
?>