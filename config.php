<?php
define('DB_HOST', 'localhost'); //hote du serveur MySQL
define('DB_USER', 'root'); //nom d'utilisateur MySQL
define('DB_PASS', ''); //mot de passe MySQL
define('DB_NAME', 'blog'); //nom base de données


try {
    $pdo = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8mb4", DB_USER, DB_PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    die("Erreur de connexion à la base de données: " . $e->getMessage());
}
?>
