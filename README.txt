	• Dans cette archive, il y a :
		• le fichier "blog.sql" qui permet de générer la base de données
		• le fichier "ProjetAPI_REST_GREGOIRE_RIOL.postman_collection" qui 
		  permettra de faire les requêtes sur Postman
		• le répertoire "ProjetAPI_REST_Final" avec les codes pour notre API
		• le rapport de notre projet

	• Pour utiliser l'API, il faudra d'abord générer un JSON Web Token avec le serveur 
	  d'authentification " jwt.php ".

	• Il y a 3 comptes dans notre base de données afin de tester notre API : 
		• Un compte modérator : username "moderator" et password "moderator"
		• Un compte publisher : username "publisher" et password "publisher"
		• Un compte publisher : username "publisher2" et password "publisher2"

	• L'URL pour tester l'API est :
		" http://localhost/www/ProjetAPI_REST_Final/serveurREST.php "
	
	• L'URL pour générer un token est :
		" http://localhost/www/ProjetAPI_REST_Final/jwt.php "

	• La collection Postman jointe possède 10 requêtes qui permettent de tester
	  les fonctions de chaque utilisateur "moderator" , "publisher" et "non 
	  authentifié ".

	• Pour tester les reqûetes de "like" et "dislike" pour un publisher, on se
	  on se connecte avec le compte "publisher" ce qui permettra de liker et disliker
	  les articles avec les id 4 et 5 écrit par "publisher2".

	• Il faudra changé le token dans la rubrique "Authorization" pour tester les
	  fonctionnalités de chaque rôle.

	
	

 