<?php
session_start();
require_once 'config.php';
require_once 'jwt_utils.php';
require_once 'fonctions.php';

header("Content-Type:application/json");

$http_method = $_SERVER['REQUEST_METHOD'];


//pour avoir le put qui fonctionne
function get_put_data() {
    parse_str(file_get_contents('php://input'), $put_data);
    return $put_data;
}

$jwt_token = get_bearer_token();
if ($jwt_token && is_jwt_valid($jwt_token)) {
    $decoded_jwt = decode_jwt($jwt_token);
    $_SESSION['user_id'] = $decoded_jwt->id;
    $_SESSION['role'] = $decoded_jwt->role;
} else {
    $_SESSION = array();
}

//pour eviter d'avoir l'erreur du non authentifié
$session_role = isset($_SESSION['role']) ? $_SESSION['role'] : null;

switch ($http_method) {
    case "GET":
        if (isset($_GET['id_article'])) {
            $article = recuperer_article($pdo, $_GET['id_article'], $session_role);
            if ($article) {
                deliver_response(200, "Article trouvé", $article);
            } else {
                deliver_response(404, "Article non trouvé", NULL);
            }
        } else {
            $articles = lister_articles($pdo, $session_role);
            deliver_response(200, "Liste des articles", $articles);
        }
        break;
    
    case "POST":
        if ($session_role == 'publisher') {
            if (isset($_GET['action'])) {
                if ($_GET['action'] == 'like') {
                    $result = liker_article($pdo, $_GET['id_article'], $_SESSION['user_id'], true);
                    if ($result === true) {
                        deliver_response(200, "Article liké", NULL);
                    } elseif ($result === false) {
                        deliver_response(400, "Impossible de liker l'article", NULL);
                    } else {
                        deliver_response(400, "Vous ne pouvez pas liker votre propre article", NULL);
                    }
                } elseif ($_GET['action'] == 'dislike') {
                    $result = disliker_article($pdo, $_GET['id_article'], $_SESSION['user_id']);
                    if ($result === true) {
                        deliver_response(200, "Article disliké", NULL);
                    } elseif ($result === false) {
                        deliver_response(400, "Impossible de disliker l'article", NULL);
                    } else {
                        deliver_response(400, "Vous ne pouvez pas disliker votre propre article", NULL);
                    }
                }
            } else {
                if (creer_article($pdo, $_POST)) {
                    deliver_response(201, "Article créé", NULL);
                } else {
                    deliver_response(400, "Impossible de créer l'article", NULL);
                }
            }
        }
        break;

    case "PUT":
        if ($session_role == 'publisher') {
            $put_data = get_put_data();
            if (modifier_article($pdo, $_GET['id_article'], $put_data)) {
                deliver_response(200, "Article mis à jour", NULL);
            } else {
                deliver_response(400, "Impossible de mettre à jour l'article", NULL);
            }
        }
        break;


    case "DELETE":
        if ($session_role == 'publisher' || $session_role == 'moderator') {
            if (supprimer_article($pdo, $_GET['id_article'], $session_role)) {
                deliver_response(200, "Article supprimé", NULL);
            } else {
                deliver_response(400, "Impossible de supprimer l'article", NULL);
            }
        }
        break;

    default:
        deliver_response(405, "Méthode non autorisée", NULL);
        break;
}

function deliver_response($status, $status_message, $data) {
    header("HTTP/1.1 $status $status_message");

    $response = new stdClass();
    $response->status = $status;
    $response->status_message = $status_message;
    $response->data = $data;

    echo json_encode($response);
}
