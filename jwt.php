<?php 
require_once 'config.php';
require_once 'jwt_utils.php';

$username = $_POST['username'];
$password = $_POST['password'];


$query = "SELECT * FROM Utilisateur WHERE NomUtilisateur = ?";
$stmt = $pdo->prepare($query);
$stmt->execute([$username]);
$user = $stmt->fetch();


if ($user && password_verify($password, $user['MotDePasse'])) {
    $payload = [
        'id' => $user['IdUtilisateur'],
        'role' => $user['Role'],
        'exp' => time() + 3600
    ];
    $jwt = generate_jwt(['alg' => 'HS256', 'typ' => 'JWT'], $payload, 'secret');
    deliver_response(200, 'JWT cree avec succes', ['token' => $jwt]);
} else {
    deliver_response(401, 'Nom d\'utilisateur ou mot de passe incorrect', null);
}

function deliver_response($status, $status_message, $data) {
    header("HTTP/1.1 $status $status_message");
    $response['status'] = $status;
    $response['status_message'] = $status_message;
    $response['data'] = $data;
    $json_response = json_encode($response);
    echo $json_response;
}

?>